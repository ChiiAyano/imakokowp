﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Xml.Serialization;
using System.Device.Location;
using ImakokoWp.Data;

namespace ImakokoWp
{
	public static class GeoLogging
	{
		static List<GeoLogData> list = new List<GeoLogData>();
		static readonly string fileName = ((App)App.Current).GpsLogFileName;
		static readonly string extension = ((App)App.Current).GpsLogFileExtension;

		/// <summary>
		/// 位置情報を保存します。
		/// </summary>
		/// <param name="lat"></param>
		/// <param name="lon"></param>
		/// <param name="altitude"></param>
		/// <param name="speed"></param>
		/// <param name="direction"></param>
		public static void Write(double lat, double lon, double altitude, double speed, double direction)
		{
			var log = new GeoLogData
			{
				Date = DateTime.Now,
				Latitude = lat,
				Longitude = lon,
				Altitude = altitude,
				Speed = speed,
				Direction = direction
			};

			GeoLogging.list.Add(log);
		}

		/// <summary>
		/// 位置情報を保存します。
		/// </summary>
		/// <param name="geoPosition"></param>
		public static void Write(GeoCoordinate geoPosition)
		{
			Write(geoPosition.Latitude, geoPosition.Longitude, geoPosition.Altitude, geoPosition.Speed, geoPosition.Course);
		}

		/// <summary>
		/// 保存した位置情報を分離ストレージに書き込みます。保存した位置情報は削除します。
		/// </summary>
		public static void Save()
		{
			if (GeoLogging.list.Count <= 0)
				return;

			var doc = GpxManager.CreateGpx(GeoLogging.list);

			using (var isf = IsolatedStorageFile.GetUserStoreForApplication())
			using (var sw = new IsolatedStorageFileStream(FileNumbering(isf), FileMode.CreateNew, isf))
			{
				doc.Save(sw);
			}

			GeoLogging.list.Clear();
		}

		private static string FileNumbering(IsolatedStorageFile isf)
		{
			return Enumerable.Range(1, short.MaxValue)
				.Select(s => DateTime.Now.ToString(string.Format(GeoLogging.fileName, s)) + GeoLogging.extension)
				.Where(w => !isf.FileExists(w))
				.FirstOrDefault();
		}
	}
}
