﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace ImakokoWp.Converter
{
	public class SpeedConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var speed = (double)value;
			var param = parameter as string;
			if (!(value is double))
				return value.ToString();
			if (double.IsNaN(speed))
				return "00.0";

			speed = speed * 3600;
			speed = speed / 1000;

			if (param == "unit")
				return speed.ToString("00.0 km/h");
			else
				return speed.ToString("00.0");
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
