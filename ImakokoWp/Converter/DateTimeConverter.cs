﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace ImakokoWp.Converter
{
	public class DateTimeConverter:IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var d = value as DateTime?;
			var p = parameter as string;

			if (d.HasValue)
			{
				if (string.IsNullOrWhiteSpace(p))
					return DateTime.Now.ToString();
				else
					return DateTime.Now.ToString(p);
			}
			else
				return value.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
