﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Device.Location;

namespace ImakokoWp.Converter
{
	public class GeoStatusConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (!(value is GeoPositionStatus))
				return value.ToString();

			switch ((GeoPositionStatus)value)
			{
				case GeoPositionStatus.Disabled:
					return "無効";
				case GeoPositionStatus.Initializing:
					return "位置の取得試行中";
				case GeoPositionStatus.NoData:
					return "位置取得失敗";
				case GeoPositionStatus.Ready:
					return "位置取得中";
				default:
					return "不明";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
