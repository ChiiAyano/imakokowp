﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Device.Location;
using System.Collections.Generic;
using ImakokoWp.Extensions;

namespace ImakokoWp.Converter
{
	public class GeoTrackConverter : IValueConverter
	{

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value is GeoCoordinate)
			{
				return value;
			}
			else if (value is IEnumerable<GeoCoordinate>)
			{
				return (value as IEnumerable<GeoCoordinate>).ToCoordinates();
			}

			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
