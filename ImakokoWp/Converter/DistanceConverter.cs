﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace ImakokoWp.Converter
{
	public class DistanceConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (!(value is double))
				return value.ToString();

			var distance = (double)value;

			if (double.IsNaN(distance))
				return "0" + "m";

			if (distance <= 999)
				return distance.ToString("0") + " m";

			distance = distance / 1000;
			return distance.ToString("0.00") + " km";
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
