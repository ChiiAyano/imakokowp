﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;

namespace ImakokoWp.Converter
{
	public class LogTypeConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (!(value is LogType))
				return value.ToString();

			switch ((LogType)value)
			{
				case LogType.AutoPost:
					return "自動Post";
				case LogType.AutoTweet:
					return "自動住所Tweet";
				case LogType.Address:
					return "住所取得";
				default:
					return value.ToString();
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
