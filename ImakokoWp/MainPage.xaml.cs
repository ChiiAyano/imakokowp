﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using ImakokoWp.Imakoko;
using Microsoft.Phone.Reactive;
using System.Device.Location;
using Microsoft.Phone.Controls.Maps;
using Microsoft.Phone.Shell;
using System.Diagnostics;
using Codeplex.OAuth;
using ImakokoWp.Data;
using Microsoft.Devices.Sensors;

namespace ImakokoWp
{
    public partial class MainPage : PhoneApplicationPage
    {
		/// <summary>
		/// 現在の参加ユーザーの位置を表すピンのリスト
		/// </summary>
		List<Pushpin> userpins;
		/// <summary>
		/// 位置情報取得
		/// </summary>
		IGeoPositionWatcher<GeoCoordinate> geo;
		/// <summary>
		/// 現在の位置情報を示すデータ
		/// </summary>
		GeoData gData = new GeoData();
		/// <summary>
		/// 現在の位置
		/// </summary>
		GeoCoordinate currentLocation;
		/// <summary>
		/// ひとつ前の位置
		/// </summary>
		GeoCoordinate previousLocation;

		/// <summary>
		/// 今ココクライアント
		/// </summary>
		ImakokoClient imakoko;

		/// <summary>
		/// 現在の累計距離
		/// </summary>
		double amountDistance = 0;

		/// <summary>
		/// 自動位置送信タイマー
		/// </summary>
		IDisposable autoPostTimer = null;
		/// <summary>
		/// 自動住所ツイートタイマー
		/// </summary>
		IDisposable autoTweetTimer = null;
		/// <summary>
		/// 以前ツイートした住所
		/// </summary>
		string previousAddress = string.Empty;

		/// <summary>
		/// ツイートに失敗したときにリトライする時間
		/// </summary>
		readonly TimeSpan autoTweetIntervalRetry = TimeSpan.FromMinutes(2);
		DateTime autoTweetNextDate;

		Settings settings;

		bool geoLogging = false;

		bool enableLocus = true;

		Stopwatch loggingWatch;

		LocationCollection geoTracks = new LocationCollection();

		/// <summary>
		/// コンパス
		/// </summary>
		Compass compass;
		/// <summary>
		/// コンパスの前の値
		/// </summary>
		double beforeHeading = 0;

        public MainPage()
        {
            InitializeComponent();
			this.DataContext = this.gData;
			this.settings = Settings.Instance;
			this.loggingWatch = new Stopwatch();

			this.autoTweetNextDate = DateTime.Now + Settings.Instance.AutoTweetInterval;

			// 地図の初期位置
			this.currentLocation = new GeoCoordinate(35.681111, 139.766667);
			this.map.Center = this.currentLocation;
			this.map.ScaleVisibility = System.Windows.Visibility.Visible;
			this.map.ZoomLevel = 16;
			this.gData.IsMapCentering = true;
        }

		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{
			// 今ココにログイン
			this.imakoko = new ImakokoClient { UserName = this.settings.ImakokoUserName, Password = this.settings.ImakokoPassword };

			if (this.currentLocation != null)
				GetAddress(this.currentLocation.Latitude, this.currentLocation.Longitude);

			// スクリーンロックをするかどうか
			try
			{
				if (this.settings.IsEnableScreenLock)
				{
					PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Enabled;
					//PhoneApplicationService.Current.ApplicationIdleDetectionMode = IdleDetectionMode.Enabled;
				}
				else
				{
					PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
					//PhoneApplicationService.Current.ApplicationIdleDetectionMode = IdleDetectionMode.Disabled;
				}
			}
			catch
			{

			}

			// コンパスを使う
			if (Compass.IsSupported)
			{
				this.compassGrid.Visibility = System.Windows.Visibility.Visible;

				this.compass = new Compass();
				this.compass.CurrentValueChanged += new EventHandler<SensorReadingEventArgs<CompassReading>>(compass_CurrentValueChanged);
				this.compass.Start();
			}
			else
			{
				this.compassGrid.Visibility = System.Windows.Visibility.Collapsed;
			}

			base.OnNavigatedTo(e);
		}

		protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
		{
			if (Compass.IsSupported && this.compass != null)
			{
				this.compass.Stop();
			}

			base.OnNavigatedFrom(e);
		}

		private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
		{
			if (this.settings.IsFirstLaunch)
			{
				// 初回起動時でメッセージ流す
				this.settings.IsFirstLaunch = false;

				var caption = ((App)Application.Current).SoftwareName + "へようこそ！";
				var message = "すべての機能を利用するには、画面下の [設定] から、「今ココなう！」アカウント、Twitterアカウント情報を入力してください。\n" +
					"「今ココなう！」アカウントが未設定の場合は今ココ機能と現在地ツイート機能が、Twitterアカウントが未設定の場合は現在地ツイート機能が使えません。\n" +
					"\n" +
					"位置情報サービスを利用しますので、設定ページの注意事項をよく読み、納得の上有効にしてください。また、システムの設定でも位置情報サービスを有効にしてください！";

				MessageBox.Show(message, caption, MessageBoxButton.OK);
			}

			this.geo = new GeoCoordinateWatcher(GeoPositionAccuracy.High);
			((GeoCoordinateWatcher)this.geo).MovementThreshold = 1;

			// 位置情報デバイスの状態取得
			Observable.FromEvent<GeoPositionStatusChangedEventArgs>(
				ev => this.geo.StatusChanged += ev,
				ev => this.geo.StatusChanged -= ev)
				.SubscribeOnDispatcher()
				.Subscribe(
					change =>
					{
						this.gData.GeoStatus = change.EventArgs.Status;
						if (change.EventArgs.Status == GeoPositionStatus.Ready)
						{
							this.loggingWatch.Start();
						}
					},
					ex =>
					{

					});

			Observable.FromEvent<GeoPositionChangedEventArgs<GeoCoordinate>>(
				ev => this.geo.PositionChanged += ev,
				ev => this.geo.PositionChanged -= ev)
				.SubscribeOnDispatcher()
				.Subscribe(
					next =>
					{
						AnalyzeGeoPosition(next.EventArgs.Position);
					},
					ex =>
					{

					});

			// 平均速度を出すためのタイマー
			Observable.Interval(TimeSpan.FromSeconds(1))
				.SubscribeOnDispatcher()
				.Subscribe(
					time =>
					{
						Deployment.Current.Dispatcher.BeginInvoke(() =>
						{
							this.gData.LoggingTime = this.loggingWatch.Elapsed;
							this.gData.AverageSpeed = this.gData.Distance / this.gData.LoggingTime.TotalSeconds;
						});
					});

			// 位置情報の取得を開始する
			if (this.settings.IsEnableGeoCoordinate)
				this.geo.Start();
			else
			{
				this.geo.Stop();
				this.gData.GeoStatus = GeoPositionStatus.Disabled;
				// 今ココやログ取りを強制的に終了
				this.autoPostSwitch.IsChecked = false;
				this.autoTweetSwitch.IsChecked = false;
				this.loggingStartSwitch.IsChecked = false;
			}

			var imakokoEnabled = (!string.IsNullOrWhiteSpace(this.settings.ImakokoUserName)) && (!string.IsNullOrWhiteSpace(this.settings.ImakokoPassword));
			this.autoPostSwitch.IsEnabled = imakokoEnabled && this.settings.IsEnableGeoCoordinate;
			this.autoTweetSwitch.IsEnabled = (this.settings.AccessToken != null) & imakokoEnabled & this.settings.IsEnableGeoCoordinate;

		}

		private void AnalyzeGeoPosition(GeoPosition<GeoCoordinate> args)
		{
			this.currentLocation = args.Location;

			this.gData.Speed = this.currentLocation.Speed;

			if (this.previousLocation != null)
			{
				// 半径2m以内の移動であれば移動したと見なさない
				if (this.previousLocation.GetDistanceTo(this.currentLocation) <= 2)
					return;

				// 距離を取って加算
				var distance = currentLocation.GetDistanceTo(this.previousLocation);
				this.gData.Distance += distance;
				this.amountDistance += distance;

				// ログとり
				if (this.geoLogging)
					GeoLogging.Write(this.currentLocation);
			}

			this.gData.Latitude = this.currentLocation.Latitude;
			this.gData.Longitude = this.currentLocation.Longitude;
			this.gData.Direction = this.currentLocation.Course;
			this.gData.Altitude = this.currentLocation.Altitude;


			this.previousLocation = this.currentLocation;

			if (this.amountDistance >= 500 || this.amountDistance == 0)
			{
				GetLatest(this.currentLocation.Latitude, this.currentLocation.Longitude);
				GetAddress(this.currentLocation.Latitude, this.currentLocation.Longitude);
				this.amountDistance = 0;
			}

			// ピンを置く
			PinMyPosition(currentLocation.Latitude, currentLocation.Longitude);
			// 軌跡を描く
			WriteMapTrack(this.currentLocation.Latitude, this.currentLocation.Longitude);

			// マップの中心を現在地に
			if (this.gData.IsMapCentering)
				this.map.Center = this.currentLocation;
		}

		Pushpin beforePin;
		private void PinMyPosition(double lat, double lon)
		{
			Deployment.Current.Dispatcher.BeginInvoke(() =>
				{
					var location =  new GeoCoordinate(lat, lon);

					var pin = new Pushpin
					{
						Style = (Style)(Application.Current.Resources["ArrowPushpin"]),
						PositionOrigin = PositionOrigin.Center,
						Location = location
					};

					pin.RenderTransform = new RotateTransform
					{
						Angle = double.IsNaN(this.gData.Direction) ? 0 : this.gData.Direction,
						CenterX = 24,
						CenterY = 24
					};

					if (this.beforePin != null)
						this.map.Children.Remove(this.beforePin);

					this.map.Children.Add(pin);
					this.beforePin = pin;
				});
		}

		private void WriteMapTrack(double lat, double lon)
		{
			if (this.enableLocus)
			{
				var loc = new GeoCoordinate(lat, lon);

				this.geoTracks.Add(loc);

				this.mapPolyline.Locations = this.geoTracks;
			}
		}

		private void GetAddress(double lat, double lon)
		{
			this.imakoko.GetAddressAsObservable(lat, lon)
				.SubscribeOnDispatcher()
				.Subscribe(
					next => Deployment.Current.Dispatcher.BeginInvoke(() => this.gData.Address = next),
					error => Deployment.Current.Dispatcher.BeginInvoke(() =>
						{
							GetAddressFromApi(lat, lon);
						}));
		}

		private void GetAddressFromApi(double lat, double lon)
		{
			ReverseGeoLocation.GetAddress(lat, lon)
				.SubscribeOnDispatcher()
				.Subscribe(
					next => Deployment.Current.Dispatcher.BeginInvoke(() => this.gData.Address = "*" + next),
					error => Deployment.Current.Dispatcher.BeginInvoke(() =>
						{
							WriteAutoPostLog(LogType.Address, "取得失敗。");
							this.gData.Address = "取得失敗";
						}));
		}

		/// <summary>
		/// 参加中のユーザーをピンにしたりリストにしたり
		/// </summary>
		/// <param name="lat"></param>
		/// <param name="lon"></param>
		private void GetLatest(double lat, double lon)
		{
			// ピンを削除
			if (this.userpins != null)
			{
				foreach (var pin in this.userpins)
				{
					this.map.Children.Remove(pin);
				}
			}

			var pins = new List<Pushpin>();
			var latest = new List<Latest>();

			this.imakoko.LatestAsObservable(lat, lon)
				.SubscribeOnDispatcher()
				.Subscribe(
					next =>
					{
						Deployment.Current.Dispatcher.BeginInvoke(() =>
						{
							latest.Add(next);

							var pin = new Pushpin
							{
								Style = (Style)(Application.Current.Resources["ArrowPushpin"]),
								PositionOrigin = PositionOrigin.Center,
								RenderTransform = new RotateTransform
								{
									Angle = next.Direction,
									CenterX = 24,
									CenterY = 24
								},
								Location = new GeoCoordinate(next.Latitude, next.Longitude),
								Content = next.Nickname
							};
							this.map.Children.Add(pin);
							pins.Add(pin);
						});
					},
					ex =>
					{

					},
					() =>
					{
						this.userpins = pins;
						Deployment.Current.Dispatcher.BeginInvoke(() =>
							{
								latest.Sort((a, b) => (int)(a.Distance - b.Distance) * 100);
								this.latestListBox.ItemsSource = latest;
								this.latestCountBlock.Text = latest.Count + "人が参加しています。";
							});
					});				
		}

		

		private void WriteAutoPostLog(LogType type, string message)
		{
			Deployment.Current.Dispatcher.BeginInvoke(() =>
			{
				var log = new AutoPostLogData
				{
					Date = DateTime.Now,
					LogType = type,
					Message = message
				};

				this.autoPostLogList.Items.Insert(0, log);
			});
		}

		private void ResetDistanceButton_Click(object sender, RoutedEventArgs e)
		{
			if (this.gData.Distance > 0)
				this.gData.Distance = 0;

			// 軌跡も消しとく
			this.geoTracks.Clear();

			var watchRunning = this.loggingWatch.IsRunning;
			this.loggingWatch.Reset();
			if (watchRunning)
				this.loggingWatch.Start();
		}

		private void autoPostSwitch_Checked(object sender, System.Windows.RoutedEventArgs e)
		{
			var sw = sender as ToggleSwitch;

			// 自動Post有効時にはPostしまくる
			if (sw.IsChecked.Value)
			{
				this.autoPostTimer = Observable.Interval(Settings.Instance.AutoPostInterval)
				.SubscribeOnDispatcher()
				.Subscribe(
					next =>
					{
						this.imakoko.PostAsObservable(
							this.currentLocation.Latitude,
							this.currentLocation.Longitude,
							this.currentLocation.Altitude,
							this.currentLocation.Course,
							this.currentLocation.Speed,
							MarkerType.Arrow)
							.SubscribeOnDispatcher()
							.Subscribe(
								s =>
								{
									Debug.WriteLine(DateTime.Now.ToString("HH:mm:dd") + ": " + s);
								},
								ex =>
								{
									WriteAutoPostLog(LogType.AutoPost, "送信失敗。");
								},
								() =>
								{
									WriteAutoPostLog(LogType.AutoPost, "送信しました。");
								});
					});
			}
			else
			{
				if (this.autoPostTimer != null)
					this.autoPostTimer.Dispose();
			}
		}

		private void autoTweetSwitch_Checked(object sender, System.Windows.RoutedEventArgs e)
		{
			var sw = sender as ToggleSwitch;

			// 自動Post有効時にはPostしまくる
			if (sw.IsChecked.Value)
			{
				// 有効化直後はすぐ実行
				this.autoTweetNextDate = DateTime.Now;


				this.autoTweetTimer = Observable.Interval(TimeSpan.FromSeconds(1))
					.Select(s => this.gData.Address)
					.SubscribeOnDispatcher()
					.Subscribe(
						address =>
						{
							if (DateTime.Now < this.autoTweetNextDate)
								return;

							if ((address == this.previousAddress) || (string.IsNullOrWhiteSpace(address)))
							{
								// 2分後に設定してから再試行
								this.autoTweetNextDate = DateTime.Now + this.autoTweetIntervalRetry;
								return;
							}

							var client = new OAuthClient(
								Settings.TwitterConsumerKey,
								Settings.TwitterConsumerSecret,
								Settings.Instance.AccessToken)
							{
								MethodType = MethodType.Post,
								Url = "http://api.twitter.com/1/statuses/update.xml",
								Parameters = { { "status", Settings.Instance.AutoTweetHeader + address + Settings.Instance.AutoTweetFooter } },
							};
							client.GetResponseText()
								.Select(s => s)
								.SubscribeOnDispatcher()
								.Subscribe(
									n =>
									{

									},
									ex =>
									{
										WriteAutoPostLog(LogType.AutoTweet, "ツイート失敗。");
									},
									() =>
									{
										WriteAutoPostLog(LogType.AutoTweet, "ツイートしました。");
									});

							this.previousAddress = address;

							// 次のスケジュールに設定
							this.autoTweetNextDate = DateTime.Now + Settings.Instance.AutoTweetInterval;
						});
			}
			else
			{
				if (this.autoTweetTimer != null)
					this.autoTweetTimer.Dispose();
			}
		}

		private void settingMenuButton_Click(object sender, EventArgs e)
		{
			// 設定画面の表示
			this.NavigationService.Navigate(new Uri("/SettingPage.xaml", UriKind.Relative));
		}

		private void loggingStartSwitch_Checked(object sender, RoutedEventArgs e)
		{
			var sw = sender as ToggleSwitch;

			if (sw.IsChecked == false)
			{
				GeoLogging.Save();
			}

			this.geoLogging = sw.IsChecked.Value;
		}

		private void map_MapPan(object sender, MapDragEventArgs e)
		{
			this.gData.IsMapCentering = false;
		}

		private void mapCenteringButton_Click(object sender, RoutedEventArgs e)
		{
			this.gData.IsMapCentering = true;

			this.map.Center = this.currentLocation;
		}

		void compass_CurrentValueChanged(object sender, SensorReadingEventArgs<CompassReading> e)
		{
			Deployment.Current.Dispatcher.BeginInvoke(() =>
			{
				// コンパスの値が変化
				var heading = e.SensorReading.TrueHeading;
				if (!double.IsNaN(heading))
					heading *= -1;

				this.rotationAnimation.From = this.beforeHeading;
				this.rotationAnimation.To = heading;

				this.CompassTurnStoryboard.Begin();

				this.beforeHeading = heading;
			});
		}
    }
}
