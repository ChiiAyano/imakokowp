﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ImakokoWp
{
	public class AutoPostLogData
	{
		public DateTime Date { get; set; }
		public LogType LogType { get; set; }
		public string Message { get; set; }
	}

	public enum LogType
	{
		AutoPost,
		AutoTweet,
		Address
	}
}
