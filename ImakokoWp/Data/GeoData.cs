﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using ImakokoWp.Imakoko;
using System.ComponentModel;
using System.Device.Location;

namespace ImakokoWp
{
	public class GeoData : INotifyPropertyChanged
	{
		double latitude;
		double longitude;
		double speed;
		double direction;
		double distance;
		double altitude;
		string address;
		GeoPositionStatus status;
		TimeSpan loggingTime;
		double averageSpeed;
		bool isLogined;
		bool isMapCentering;

		/// <summary>
		/// 緯度を取得または設定します。
		/// </summary>
		public double Latitude
		{
			get { return this.latitude; }
			set
			{
				if (this.latitude != value)
				{
					this.latitude = value;
					RaisePropertyChanged("Latitude");
					RaisePropertyChanged("Position");
				}
			}
		}
		/// <summary>
		/// 経度を取得または設定します。
		/// </summary>
		public double Longitude
		{
			get { return this.longitude; }
			set
			{
				if (this.longitude != value)
				{
					this.longitude = value;
					RaisePropertyChanged("Longitude");
					RaisePropertyChanged("Position");
				}
			}
		}
		/// <summary>
		/// 速度を取得または設定します。
		/// </summary>
		public double Speed
		{
			get { return this.speed; }
			set
			{
				if (this.speed != value)
				{
					this.speed = value;
					RaisePropertyChanged("Speed");
				}
			}
		}
		/// <summary>
		/// 方角を取得または設定します。
		/// </summary>
		public double Direction
		{
			get { return this.direction; }
			set
			{

				if (this.direction != value)
				{
					this.direction = value;
					RaisePropertyChanged("Direction");
				}
			}
		}
		/// <summary>
		/// 移動距離を取得または設定します。
		/// </summary>
		public double Distance
		{
			get { return this.distance; }
			set
			{
				if (this.distance != value)
				{
					this.distance = value;
					RaisePropertyChanged("Distance");
				}
			}
		}
		/// <summary>
		/// 住所を取得または設定します。
		/// </summary>
		public string Address
		{
			get { return this.address; }
			set
			{
				if (this.address != value)
				{
					this.address = value;
					RaisePropertyChanged("Address");

					if (value.StartsWith("*") || value == "取得失敗")
						this.IsLogined = false;
					else
						this.IsLogined = true;
				}
			}
		}

		/// <summary>
		/// 今ココにログインできたかを取得します。
		/// </summary>
		public bool IsLogined
		{
			get { return this.isLogined; }
			set
			{
				if (this.isLogined != value)
				{
					this.isLogined = value;
					RaisePropertyChanged("IsLogined");
				}
			}
		}
		/// <summary>
		/// 現在の位置を取得します。
		/// </summary>
		public string Position
		{
			get { return Latest.FormatPosition(this.Latitude, this.Longitude); }
		}

		public double Altitude
		{
			get { return this.altitude; }
			set
			{
				if (this.altitude != value)
				{
					this.altitude = value;
					RaisePropertyChanged("Altitude");
				}
			}
		}

		/// <summary>
		/// 位置情報デバイスの状態を取得または設定します。
		/// </summary>
		public GeoPositionStatus GeoStatus
		{
			get { return this.status; }
			set
			{
				if (this.status != value)
				{
					this.status = value;
					RaisePropertyChanged("GeoStatus");
				}
			}
		}

		/// <summary>
		/// 位置情報のログを取得している時間を取得または設定します。
		/// </summary>
		public TimeSpan LoggingTime
		{
			get { return this.loggingTime; }
			set
			{
				if (this.loggingTime != value)
				{
					this.loggingTime = value;
					RaisePropertyChanged("LoggingTime");
				}
			}
		}

		public double AverageSpeed
		{
			get { return this.averageSpeed; }
			set
			{
				if (this.averageSpeed != value)
				{
					this.averageSpeed = value;
					RaisePropertyChanged("AverageSpeed");
				}
			}
		}

		/// <summary>
		/// 位置情報デバイスの状態が有効かを取得します。
		/// </summary>
		public bool IsGeoCoordinateEnabled
		{
			get { return this.GeoStatus != GeoPositionStatus.Disabled; }
		}

		/// <summary>
		/// マップの中心を自分の位置にするかどうか取得または設定します。
		/// </summary>
		public bool IsMapCentering
		{
			get { return this.isMapCentering; }
			set
			{
				if (this.isMapCentering != value)
				{
					this.isMapCentering = value;
					RaisePropertyChanged("IsMapCentering");
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected void RaisePropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
