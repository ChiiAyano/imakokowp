﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace ImakokoWp.Data
{
	public class CompassData : INotifyPropertyChanged
	{
		private bool isSupported = false;
		private double trueHeading = 0d;
		private double accuary = 0d;

		public bool IsSupported
		{
			get { return this.isSupported; }
			set
			{
				if (this.isSupported != value)
				{
					this.isSupported = value;
					RaisePropertyChanged("IsSupported");
				}
			}
		}

		public double TrueHeading
		{
			get { return this.trueHeading; }
			set
			{
				if (this.trueHeading != value)
				{
					this.trueHeading = value;
					RaisePropertyChanged("TrueHeading");
				}
			}
		}

		public double Accuary
		{
			get { return this.accuary; }
			set
			{
				if (this.accuary != value)
				{
					this.accuary = value;
					RaisePropertyChanged("Accuary");
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected void RaisePropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
