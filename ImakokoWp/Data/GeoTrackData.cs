﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Device.Location;

namespace ImakokoWp.Data
{
	/// <summary>
	/// 軌跡を表示するためのデータ セット
	/// </summary>
	public class GeoTrackData
	{
		public GeoCoordinate Track { get; set; }
	}
}
