﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ImakokoWp.Data
{
	/// <summary>
	/// GPS ログ取り用データ セット
	/// </summary>
	public class GeoLogData
	{
		public DateTime Date { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public double Altitude { get; set; }
		public double Speed { get; set; }
		public double Direction { get; set; }
	}
}
