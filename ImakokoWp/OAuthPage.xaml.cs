﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Codeplex.OAuth;
using Microsoft.Phone.Reactive;

namespace ImakokoWp
{
	public partial class OAuthPage : PhoneApplicationPage
	{
		RequestToken reqToken = null;
		Settings settings;

		public OAuthPage()
		{
			InitializeComponent();

			this.settings = Settings.Instance;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			var auth = new OAuthAuthorizer(Settings.TwitterConsumerKey,Settings.TwitterConsumerSecret);
			auth.GetAccessToken("https://api.twitter.com/oauth/access_token", this.reqToken, pinBox.Text)
					.ObserveOnDispatcher()
					.Subscribe(res =>
					{
						//General.Settings.UserId = res.ExtraData["user_id"].First();
						//General.Settings.UserName = res.ExtraData["screen_name"].First();
						//General.Settings.AccessKey = res.Token.Key;
						//General.Settings.AccessSecret = res.Token.Secret;
						//General.AccessToken = res.Token;
						//General.Save();

						this.settings.AccessToken = res.Token;
						this.settings.TwitterUserId = res.ExtraData["user_id"].First();
					},
							ex =>
							{
								MessageBox.Show("認証に失敗しました。");
								//authBrowser.Navigate(new Uri("javascript:history.back();", UriKind.RelativeOrAbsolute));
								pinBox.Text = string.Empty;
								BrowseAuthorizer();
							}
							,
							() =>
							{
								NavigationService.GoBack();
							}
							);
		}

		private void BrowseAuthorizer()
		{
			var auth = new OAuthAuthorizer(Settings.TwitterConsumerKey, Settings.TwitterConsumerSecret);
			auth.GetRequestToken("https://api.twitter.com/oauth/request_token")
					.Select(res => res.Token)
					.ObserveOnDispatcher()
					.Subscribe(token =>
					{
						this.reqToken = token;
						var authUrl = auth.BuildAuthorizeUrl("https://api.twitter.com/oauth/authorize", token);
						authBrowser.Navigate(new Uri(authUrl));
					},
					ex =>
					{
						MessageBox.Show("認証の準備に失敗しました。" + ex.Message);
					},
					() =>
					{

					});
		}

		private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
		{
			BrowseAuthorizer();
		}

		private void authBrowser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
		{
			if (e.Uri == new Uri("https://api.twitter.com/oauth/authorize"))
				pinBox.Focus();
		}

		private void pinBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (pinBox.Text.Length > 0)
				authButton.IsEnabled = true;
			else
				authButton.IsEnabled = false;
		}

	}
}