﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Codeplex.OAuth;
using Microsoft.Phone.Reactive;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;

namespace ImakokoWp.Twitter
{
	public class Twitter
	{
		public AccessToken AccessToken { get; set; }

		public Twitter(AccessToken accessToken)
		{
			this.AccessToken = accessToken;
		}

		/// <summary>
		/// 指定されたユーザー ID の情報を取得します。
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public IObservable<IEnumerable<UserShowData>> UserShow(string userId)
		{
			var client = new OAuthClient(Settings.TwitterConsumerKey, Settings.TwitterConsumerSecret, this.AccessToken)
			{
				Url = "https://api.twitter.com/1/users/show.xml",
				Parameters = { { "user_id", userId } }
			};

			return client.GetResponseText()
				.Select(s => XDocument.Parse(s).Descendants("user"))
				.Select(s => ToUserShow(s));
		}

		private IEnumerable<UserShowData> ToUserShow(IEnumerable<XElement> elements)
		{
			var users = elements.Select(s => new UserShowData
				{
					UserId = s.Element("id").Value,
					ScreenName = s.Element("screen_name").Value,
					NickName = s.Element("name").Value,
					ImageUri = new Uri(s.Element("profile_image_url").Value)
				});
			return users;
		}
	}
}
