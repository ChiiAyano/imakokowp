﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ImakokoWp.Twitter
{
	public class UserShowData
	{
		public string UserId { get; set; }
		public string ScreenName { get; set; }
		public string NickName { get; set; }
		public Uri ImageUri { get; set; }
	}
}
