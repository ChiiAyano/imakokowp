﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Codeplex.OAuth;
using System.IO.IsolatedStorage;
using System.Diagnostics;

namespace ImakokoWp
{
	public class Settings
	{
		static Settings instance = null;
		static IsolatedStorageSettings storage = IsolatedStorageSettings.ApplicationSettings;

		static string version = "0.1 (2012/07/19)";

		/// <summary>
		/// Twitter で使用する OAuth 認証用の Consumer Key
		/// </summary>
		public const string TwitterConsumerKey = "wwMxMBFBdwBRuzGzWkb2g";
		/// <summary>
		/// Twitter で使用する OAuth 認証用の Consumer Secret
		/// </summary>
		public const string TwitterConsumerSecret = "CaNP6hdEz2yyVKg4AwvGufqk16k9tUr45UssHhItJk";

		

		private Settings()
		{

		}

		#region 静的プロパティ

		/// <summary>
		/// このクラスのインスタンスを返します。
		/// </summary>
		public static Settings Instance
		{
			get
			{
				if (Settings.instance == null)
					Settings.instance = new Settings();

				return Settings.instance;
			}
		}

		/// <summary>
		/// このソフトウェアのバージョンを取得します。
		/// </summary>
		public static string Version
		{
			get { return Settings.version; }
		}

		#endregion

		#region 設定項目

		public bool IsFirstLaunch
		{
			get { return GetValueOrDefault("firstlaunch", true); }
			set { AddOrUpdateValue("firstlaunch", value); }
		}

		/// <summary>
		/// Twitter で使用する OAuth 認証用の Access Token を取得または設定します。
		/// </summary>
		public AccessToken AccessToken
		{
			get
			{
				var akey = GetValueOrDefault("acckey", string.Empty);
				var asec = GetValueOrDefault("accsec", string.Empty);

				if (string.IsNullOrWhiteSpace(akey) || string.IsNullOrWhiteSpace(asec))
					return null;

				return new AccessToken(akey, asec);
			}
			set
			{
				AddOrUpdateValue("acckey", value.Key);
				AddOrUpdateValue("accsec", value.Secret);
			}
		}

		/// <summary>
		/// Twitter のユーザー ID を取得または設定します。
		/// </summary>
		public string TwitterUserId
		{
			get { return GetValueOrDefault("twituser", string.Empty); }
			set { AddOrUpdateValue("twituser", value); }
		}
		/// <summary>
		/// 今ココなう！のユーザー名を取得または設定します。
		/// </summary>
		public string ImakokoUserName
		{
			get { return GetValueOrDefault("ikuser", string.Empty); }
			set { AddOrUpdateValue("ikuser", value); }
		}
		/// <summary>
		/// 今ココなう！のパスワードを取得または設定します。
		/// </summary>
		public string ImakokoPassword
		{
			get { return GetValueOrDefault("ikpass", string.Empty); }
			set { AddOrUpdateValue("ikpass", value); }
		}
		/// <summary>
		/// 今ココなう！で自動 Post する間隔を取得または設定します。
		/// </summary>
		public TimeSpan AutoPostInterval
		{
			get { return GetValueOrDefault("autopost", TimeSpan.FromSeconds(10)); }
			set { AddOrUpdateValue("autopost", value); }
		}
		/// <summary>
		/// 今ココなう！で自動住所ツイートする間隔を取得または設定します。
		/// </summary>
		public TimeSpan AutoTweetInterval
		{
			get { return GetValueOrDefault("autotweet", TimeSpan.FromMinutes(10)); }
			set { AddOrUpdateValue("autotweet", value); }
		}

		/// <summary>
		/// 今ココなう！で自動住所ツイートする際のヘッダーを取得または設定します。
		/// </summary>
		public string AutoTweetHeader
		{
			get { return GetValueOrDefault("tweetheader", "ｲﾏｺｺ! L: "); }
			set { AddOrUpdateValue("tweetheader", value); }
		}

		/// <summary>
		/// 今ココなう！で自動住所ツイートする際のフッターを取得または設定します。
		/// </summary>
		public string AutoTweetFooter
		{
			get { return GetValueOrDefault("tweetfooter", string.Empty); }
			set { AddOrUpdateValue("tweetfooter", value); }
		}

		/// <summary>
		/// 位置情報サービスが有効かどうかを取得または設定します。
		/// </summary>
		public bool IsEnableGeoCoordinate
		{
			get { return GetValueOrDefault("geocoordinate", false); }
			set { AddOrUpdateValue("geocoordinate", value); }
		}

		/// <summary>
		/// スクリーンロックを有効にするか取得または設定します。
		/// </summary>
		public bool IsEnableScreenLock
		{
			get { return GetValueOrDefault("screenlock", true); }
			set { AddOrUpdateValue("screenlock", value); }
		}

		#endregion

		/// <summary>
		/// アプリケーションの設定を追加します。キーが存在する場合は設定を更新します。
		/// </summary>
		/// <param name="key">追加または更新するキー</param>
		/// <param name="value">追加または更新する値</param>
		/// <returns></returns>
		private static bool AddOrUpdateValue(string key, object value)
		{
			var valueChanged = false;

			try
			{
				if (Settings.storage.Contains(key))
				{
					if (Settings.storage[key] != value)
					{
						Settings.storage[key] = value;
						valueChanged = true;
					}
				}
				else
				{
					Settings.storage.Add(key, value);
					valueChanged = true;
				}
			}
			catch (Exception ex)
			{
				Debug.WriteLine("IsolatedSettings Exception: " + ex.Message);
			}

			return valueChanged;
		}

		/// <summary>
		/// アプリケーションの設定を取得します。キーが存在しない場合は既定値を返します。
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="key">取得するキー</param>
		/// <param name="defaultValue">キーが存在しなかった場合の既定値</param>
		/// <returns></returns>
		private static T GetValueOrDefault<T>(string key, T defaultValue)
		{
			T value;

			if (Settings.storage.TryGetValue(key, out value))
			{
				return value;
			}
			else
			{
				return defaultValue;
			}
		}
	}
}
