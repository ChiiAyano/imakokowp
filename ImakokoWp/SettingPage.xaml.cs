﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Reactive;
using System.Windows.Media.Imaging;
using System.IO;
using System.Device.Location;
using Microsoft.Phone.Tasks;

namespace ImakokoWp
{
    public partial class SettingPage : PhoneApplicationPage
    {
		Settings settings;

        public SettingPage()
        {
            InitializeComponent();
        }

		private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
		{
			var app = ((App)Application.Current);
			this.softwareNameBlock.Text = app.SoftwareName;
			this.versionBlock.Text = app.SoftwareVersion;
		}

		protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
		{
			if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back)
			{
				// めんどくさいから各設定への反映はいつも通りで
				this.settings.AutoPostInterval = GetPostIntervalFromPicker(this.autoPostIntervalPicker);
				this.settings.ImakokoUserName = this.imakokoUserBox.Text;
				this.settings.ImakokoPassword = this.imakokoPasswordBox.Password;
				this.settings.AutoTweetHeader = this.imakokoHeaderBox.Text;
				this.settings.AutoTweetFooter = this.imakokoFooterBox.Text;
				this.settings.AutoTweetInterval = GetTweetIntervalFromPicker(this.autoTweetIntervalPicker);
				this.settings.IsEnableGeoCoordinate = this.geocoordinateSwitch.IsChecked.Value;
				this.settings.IsEnableScreenLock = this.screenLockSwitch.IsChecked.Value;
			}

			base.OnNavigatedFrom(e);
		}

		protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
		{
			this.settings = Settings.Instance;

			// めんどくさいから各コントロールへの設定反映はいつも通りで
			this.imakokoUserBox.Text = this.settings.ImakokoUserName;
			this.imakokoPasswordBox.Password = this.settings.ImakokoPassword;
			SelectPostIntervalPicker(this.autoPostIntervalPicker, this.settings.AutoPostInterval);
			SelectTweetIntervalPicker(this.autoTweetIntervalPicker, this.settings.AutoTweetInterval);
			this.imakokoHeaderBox.Text = this.settings.AutoTweetHeader;
			this.imakokoFooterBox.Text = this.settings.AutoTweetFooter;
			this.geocoordinateSwitch.IsChecked = this.settings.IsEnableGeoCoordinate;
			this.screenLockSwitch.IsChecked = this.settings.IsEnableScreenLock;

			// OAuth認証とかの後処理
			if (this.settings.AccessToken != null)
			{
				var t = new Twitter.Twitter(this.settings.AccessToken)
					.UserShow(this.settings.TwitterUserId)
					.SubscribeOnDispatcher()
					.Subscribe(
						s =>
						{
							Deployment.Current.Dispatcher.BeginInvoke(() =>
								{
									var user = s.FirstOrDefault();
									if (user == null)
										return;

									this.twitterInfoBlock.Text =
										"User ID: " + user.UserId + "\n" +
										"Screen Name: " + user.ScreenName + "\n" +
										"Nick Name: " + user.NickName;
									this.twitterImage.Source = new BitmapImage(user.ImageUri);
								});
						},
						ex =>
						{
							var resText = string.Empty;

							using (var res = ((WebException)ex).Response.GetResponseStream())
							using (var sr = new StreamReader(res))
							{
								resText = sr.ReadToEnd();
							}

							Deployment.Current.Dispatcher.BeginInvoke(() =>
								{
									MessageBox.Show(resText);
								});
						});
			}

			// 位置情報サービスが有効かを確認
			var loc = new GeoCoordinateWatcher();
			loc.StatusChanged += (s, le) =>
				{
					if (le.Status == GeoPositionStatus.Disabled)
					{
						loc.Stop();
						this.geocoordinateSwitch.IsChecked = false;
						this.geocoordinateSwitch.IsEnabled = false;
					}
					else if (le.Status == GeoPositionStatus.Initializing)
					{ }
					else
					{
						loc.Stop();
						this.geocoordinateSwitch.IsEnabled = true;
					}
				};
			loc.Start();

			base.OnNavigatedTo(e);
		}

		private void SelectPostIntervalPicker(ListPicker picker, TimeSpan interval)
		{
			if (interval == TimeSpan.FromSeconds(5))
				picker.SelectedIndex = 0;
			else if (interval == TimeSpan.FromSeconds(10))
				picker.SelectedIndex = 1;
			else if (interval == TimeSpan.FromSeconds(20))
				picker.SelectedIndex = 2;
			else if (interval == TimeSpan.FromSeconds(30))
				picker.SelectedIndex = 3;
			else
				picker.SelectedIndex = 1;
		}

		private void SelectTweetIntervalPicker(ListPicker picker, TimeSpan interval)
		{
			if (interval == TimeSpan.FromMinutes(1))
				picker.SelectedIndex = 0;
			else if (interval == TimeSpan.FromMinutes(3))
				picker.SelectedIndex = 1;
			else if (interval == TimeSpan.FromMinutes(5))
				picker.SelectedIndex = 2;
			else if (interval == TimeSpan.FromMinutes(10))
				picker.SelectedIndex = 3;
			else
				picker.SelectedIndex = 3;
		}

		private TimeSpan GetPostIntervalFromPicker(ListPicker picker)
		{
			switch (picker.SelectedIndex)
			{
				case 0:
					return TimeSpan.FromSeconds(5);
				case 1:
					return TimeSpan.FromSeconds(10);
				case 2:
					return TimeSpan.FromSeconds(20);
				case 3:
					return TimeSpan.FromSeconds(30);
				case 4:
					return TimeSpan.FromSeconds(60);
				default:
					return TimeSpan.FromSeconds(10);
			}
		}

		private TimeSpan GetTweetIntervalFromPicker(ListPicker picker)
		{
			switch (picker.SelectedIndex)
			{
				case 0:
					return TimeSpan.FromMinutes(1);
				case 1:
					return TimeSpan.FromMinutes(3);
				case 2:
					return TimeSpan.FromMinutes(5);
				case 3:
					return TimeSpan.FromMinutes(10);
				default:
					return TimeSpan.FromMinutes(10);
			}
		}

		private void twitterOAuthButton_Click(object sender, RoutedEventArgs e)
		{
			this.NavigationService.Navigate(new Uri("/OAuthPage.xaml", UriKind.Relative));
		}

		#region About

		private void urlLink_Click(object sender, RoutedEventArgs e)
		{
			WebBrowserTask task = new WebBrowserTask()
			{
				Uri = new Uri(this.urlLink.Content as string)
			};

			task.Show();
		}

		private void mailLink_Click(object sender, RoutedEventArgs e)
		{
			EmailComposeTask task = new EmailComposeTask()
			{
				To = this.mailLink.Content as string,
				Subject = "[ShortcutDialer] "
			};

			task.Show();
		}

		private void reviewButton_Click(object sender, RoutedEventArgs e)
		{
			MarketplaceReviewTask marketplace = new MarketplaceReviewTask();
			marketplace.Show();
		}

		#endregion

	}
}
