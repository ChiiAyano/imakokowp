﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Linq;
using ImakokoWp.Data;

namespace ImakokoWp
{
	public static class GpxManager
	{
		/// <summary>
		/// GPX ファイルを作成します。
		/// </summary>
		/// <param name="log"></param>
		/// <returns></returns>
		public static XDocument CreateGpx(IEnumerable<GeoLogData> log)
		{
			XNamespace ns = "http://www.topografix.com/GPX/1/1";
			XNamespace xml = "http://www.w3.org/XML/1998/namespace";
			XNamespace xsd = "http://www.w3.org/2001/XMLSchema";

			var root = new XElement(ns + "gpx",
				new XAttribute("version", "1.1"),
				new XAttribute("creator", ((App)App.Current).SoftwareNameEn + " " + ((App)App.Current).SoftwareVersion),
				new XAttribute(XNamespace.Xmlns + "xml", xml),
				new XAttribute(XNamespace.Xmlns + "xsd", xsd));

			var trkpt = log.Select(s =>
			{
				var e = new XElement(ns + "trkpt", new XAttribute("lat", s.Latitude), new XAttribute("lon", s.Longitude),
					new XElement(ns + "ele", s.Altitude),
					new XElement(ns + "time", s.Date));
				return e;
			});

			root.Add(new XElement(ns + "trk", new XElement("trkseg", trkpt)));

			return new XDocument(root);
		}

		/// <summary>
		/// GPX を読み込みます。
		/// </summary>
		/// <param name="gpx"></param>
		/// <returns></returns>
		public static IEnumerable<GeoLogData> Read(XDocument gpx)
		{
			var log = gpx.Descendants("trkpt")
				.Select(s => new GeoLogData
				{
					Latitude = double.Parse(s.Attribute("lat").Value),
					Longitude = double.Parse(s.Attribute("lon").Value),
					Altitude = double.Parse(s.Element("ele").Value)
				});

			return log;
		}
	}
}
