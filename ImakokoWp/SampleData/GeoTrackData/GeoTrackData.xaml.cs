﻿//      *********    このファイルを編集しないでください     *********
//      このファイルはデザイン ツールにより作成されました。
//      このファイルに変更を加えるとエラーが発生する場合があります。
namespace Expression.Blend.SampleData.GeoTrackData
{
	using System; 

// To significantly reduce the sample data footprint in your production application, you can set
// the DISABLE_SAMPLE_DATA conditional compilation constant and disable sample data at runtime.
#if DISABLE_SAMPLE_DATA
	internal class GeoTrackData { }
#else

	public class GeoTrackData : System.ComponentModel.INotifyPropertyChanged
	{
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}

		public GeoTrackData()
		{
			try
			{
				System.Uri resourceUri = new System.Uri("/ImakokoWp;component/SampleData/GeoTrackData/GeoTrackData.xaml", System.UriKind.Relative);
				if (System.Windows.Application.GetResourceStream(resourceUri) != null)
				{
					System.Windows.Application.LoadComponent(this, resourceUri);
				}
			}
			catch (System.Exception)
			{
			}
		}

		private GeoTracks _GeoTracks = new GeoTracks();

		public GeoTracks GeoTracks
		{
			get
			{
				return this._GeoTracks;
			}
		}
	}

	public class GeoTracksItem : System.ComponentModel.INotifyPropertyChanged
	{
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}

		private string _Track = string.Empty;

		public string Track
		{
			get
			{
				return this._Track;
			}

			set
			{
				if (this._Track != value)
				{
					this._Track = value;
					this.OnPropertyChanged("Track");
				}
			}
		}
	}

	public class GeoTracks : System.Collections.ObjectModel.ObservableCollection<GeoTracksItem>
	{ 
	}
#endif
}
