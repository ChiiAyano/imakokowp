﻿//      *********    このファイルを編集しないでください     *********
//      このファイルはデザイン ツールにより作成されました。
//      このファイルに変更を加えるとエラーが発生する場合があります。
namespace Expression.Blend.SampleData.LatestData
{
	using System; 

// To significantly reduce the sample data footprint in your production application, you can set
// the DISABLE_SAMPLE_DATA conditional compilation constant and disable sample data at runtime.
#if DISABLE_SAMPLE_DATA
	internal class LatestData { }
#else

	public class LatestData : System.ComponentModel.INotifyPropertyChanged
	{
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}

		public LatestData()
		{
			try
			{
				System.Uri resourceUri = new System.Uri("/ImakokoWp;component/SampleData/LatestData/LatestData.xaml", System.UriKind.Relative);
				if (System.Windows.Application.GetResourceStream(resourceUri) != null)
				{
					System.Windows.Application.LoadComponent(this, resourceUri);
				}
			}
			catch (System.Exception)
			{
			}
		}

		private ItemCollection _Collection = new ItemCollection();

		public ItemCollection Collection
		{
			get
			{
				return this._Collection;
			}
		}
	}

	public class Item : System.ComponentModel.INotifyPropertyChanged
	{
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}

		private double _Altitude = 0;

		public double Altitude
		{
			get
			{
				return this._Altitude;
			}

			set
			{
				if (this._Altitude != value)
				{
					this._Altitude = value;
					this.OnPropertyChanged("Altitude");
				}
			}
		}

		private double _Direction = 0;

		public double Direction
		{
			get
			{
				return this._Direction;
			}

			set
			{
				if (this._Direction != value)
				{
					this._Direction = value;
					this.OnPropertyChanged("Direction");
				}
			}
		}

		private double _Distance = 0;

		public double Distance
		{
			get
			{
				return this._Distance;
			}

			set
			{
				if (this._Distance != value)
				{
					this._Distance = value;
					this.OnPropertyChanged("Distance");
				}
			}
		}

		private double _Latitude = 0;

		public double Latitude
		{
			get
			{
				return this._Latitude;
			}

			set
			{
				if (this._Latitude != value)
				{
					this._Latitude = value;
					this.OnPropertyChanged("Latitude");
				}
			}
		}

		private double _Longitude = 0;

		public double Longitude
		{
			get
			{
				return this._Longitude;
			}

			set
			{
				if (this._Longitude != value)
				{
					this._Longitude = value;
					this.OnPropertyChanged("Longitude");
				}
			}
		}

		private string _MarkerType = string.Empty;

		public string MarkerType
		{
			get
			{
				return this._MarkerType;
			}

			set
			{
				if (this._MarkerType != value)
				{
					this._MarkerType = value;
					this.OnPropertyChanged("MarkerType");
				}
			}
		}

		private string _Nickname = string.Empty;

		public string Nickname
		{
			get
			{
				return this._Nickname;
			}

			set
			{
				if (this._Nickname != value)
				{
					this._Nickname = value;
					this.OnPropertyChanged("Nickname");
				}
			}
		}

		private string _Position = string.Empty;

		public string Position
		{
			get
			{
				return this._Position;
			}

			set
			{
				if (this._Position != value)
				{
					this._Position = value;
					this.OnPropertyChanged("Position");
				}
			}
		}

		private string _ShowName = string.Empty;

		public string ShowName
		{
			get
			{
				return this._ShowName;
			}

			set
			{
				if (this._ShowName != value)
				{
					this._ShowName = value;
					this.OnPropertyChanged("ShowName");
				}
			}
		}

		private double _Speed = 0;

		public double Speed
		{
			get
			{
				return this._Speed;
			}

			set
			{
				if (this._Speed != value)
				{
					this._Speed = value;
					this.OnPropertyChanged("Speed");
				}
			}
		}

		private string _Status = string.Empty;

		public string Status
		{
			get
			{
				return this._Status;
			}

			set
			{
				if (this._Status != value)
				{
					this._Status = value;
					this.OnPropertyChanged("Status");
				}
			}
		}

		private string _User = string.Empty;

		public string User
		{
			get
			{
				return this._User;
			}

			set
			{
				if (this._User != value)
				{
					this._User = value;
					this.OnPropertyChanged("User");
				}
			}
		}

		private string _UstreamStatus = string.Empty;

		public string UstreamStatus
		{
			get
			{
				return this._UstreamStatus;
			}

			set
			{
				if (this._UstreamStatus != value)
				{
					this._UstreamStatus = value;
					this.OnPropertyChanged("UstreamStatus");
				}
			}
		}
	}

	public class ItemCollection : System.Collections.ObjectModel.ObservableCollection<Item>
	{ 
	}
#endif
}
