﻿//      *********    このファイルを編集しないでください     *********
//      このファイルはデザイン ツールにより作成されました。
//      このファイルに変更を加えるとエラーが発生する場合があります。
namespace Expression.Blend.SampleData.GeoCoodinateData
{
	using System; 

// To significantly reduce the sample data footprint in your production application, you can set
// the DISABLE_SAMPLE_DATA conditional compilation constant and disable sample data at runtime.
#if DISABLE_SAMPLE_DATA
	internal class GeoCoodinateData { }
#else

	public class GeoCoodinateData : System.ComponentModel.INotifyPropertyChanged
	{
		public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
			{
				this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
			}
		}

		public GeoCoodinateData()
		{
			try
			{
				System.Uri resourceUri = new System.Uri("/ImakokoWp;component/SampleData/GeoCoodinateData/GeoCoodinateData.xaml", System.UriKind.Relative);
				if (System.Windows.Application.GetResourceStream(resourceUri) != null)
				{
					System.Windows.Application.LoadComponent(this, resourceUri);
				}
			}
			catch (System.Exception)
			{
			}
		}

		private double _Latitude = 0;

		public double Latitude
		{
			get
			{
				return this._Latitude;
			}

			set
			{
				if (this._Latitude != value)
				{
					this._Latitude = value;
					this.OnPropertyChanged("Latitude");
				}
			}
		}

		private string _Longitude = string.Empty;

		public string Longitude
		{
			get
			{
				return this._Longitude;
			}

			set
			{
				if (this._Longitude != value)
				{
					this._Longitude = value;
					this.OnPropertyChanged("Longitude");
				}
			}
		}

		private string _Speed = string.Empty;

		public string Speed
		{
			get
			{
				return this._Speed;
			}

			set
			{
				if (this._Speed != value)
				{
					this._Speed = value;
					this.OnPropertyChanged("Speed");
				}
			}
		}

		private string _Direction = string.Empty;

		public string Direction
		{
			get
			{
				return this._Direction;
			}

			set
			{
				if (this._Direction != value)
				{
					this._Direction = value;
					this.OnPropertyChanged("Direction");
				}
			}
		}

		private string _Distance = string.Empty;

		public string Distance
		{
			get
			{
				return this._Distance;
			}

			set
			{
				if (this._Distance != value)
				{
					this._Distance = value;
					this.OnPropertyChanged("Distance");
				}
			}
		}

		private string _Address = string.Empty;

		public string Address
		{
			get
			{
				return this._Address;
			}

			set
			{
				if (this._Address != value)
				{
					this._Address = value;
					this.OnPropertyChanged("Address");
				}
			}
		}

		private string _GeoStatus = string.Empty;

		public string GeoStatus
		{
			get
			{
				return this._GeoStatus;
			}

			set
			{
				if (this._GeoStatus != value)
				{
					this._GeoStatus = value;
					this.OnPropertyChanged("GeoStatus");
				}
			}
		}

		private string _AverageSpeed = string.Empty;

		public string AverageSpeed
		{
			get
			{
				return this._AverageSpeed;
			}

			set
			{
				if (this._AverageSpeed != value)
				{
					this._AverageSpeed = value;
					this.OnPropertyChanged("AverageSpeed");
				}
			}
		}

		private string _Altitude = string.Empty;

		public string Altitude
		{
			get
			{
				return this._Altitude;
			}

			set
			{
				if (this._Altitude != value)
				{
					this._Altitude = value;
					this.OnPropertyChanged("Altitude");
				}
			}
		}
	}
#endif
}
