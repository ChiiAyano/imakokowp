﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Reactive;
using System.IO;

namespace ImakokoWp.Extensions
{
	public static class WebRequestExtensions
	{
		/// <summary>
		/// レスポンスをオブザーバブルとして返します。
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public static IObservable<WebResponse> GetResponseAsObservable(this WebRequest request)
		{
			return Observable.FromAsyncPattern<WebResponse>(request.BeginGetResponse, request.EndGetResponse).Invoke();
		}

		/// <summary>
		/// 応答文字列をオブザーバブルとして返します。
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public static IObservable<string> GetResponseStringAsObservable(this WebRequest request)
		{
			var res = GetResponseAsObservable(request).Select(s =>
				{
					using (var sr = new StreamReader(s.GetResponseStream()))
					{
						return sr.ReadToEnd();
					}
				});
			return res;
		}
	}
}
