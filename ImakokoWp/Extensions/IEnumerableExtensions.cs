﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls.Maps;
using System.Collections;
using System.Device.Location;
using System.Collections.Generic;

namespace ImakokoWp.Extensions
{
	public static class IEnumerableExtensions
	{
		/// <summary>
		/// <see cref="GeoCoordinate"/> が含まれた列挙を <see cref="LocationCollection"/> に変換します。
		/// </summary>
		/// <param name="points"></param>
		/// <returns></returns>
		public static LocationCollection ToCoordinates(this IEnumerable<GeoCoordinate> points)
		{
			var locs = new LocationCollection();

			if (points != null)
			{
				foreach (var item in points)
				{
					locs.Add(item);
				}
			}

			return locs;
		}
	}
}
