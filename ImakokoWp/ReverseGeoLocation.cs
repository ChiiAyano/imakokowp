﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Device.Location;
using ImakokoWp.Extensions;
using System.Xml.Linq;
using System.Linq;
using Microsoft.Phone.Reactive;

namespace ImakokoWp
{
	public class ReverseGeoLocation
	{
		static Uri api = new Uri("http://geocode.didit.jp/reverse/");

		public ReverseGeoLocation()
		{
		}

		/// <summary>
		/// 位置情報を元に住所を特定します。
		/// </summary>
		/// <param name="geoData"></param>
		/// <returns></returns>
		public static IObservable<string> GetAddress(double lat, double lon)
		{
			var address = string.Empty;
			var hwres = WebRequest.Create(ReverseGeoLocation.api + "?lat=" + lat + "&lon=" + lon);
			return hwres.GetResponseStringAsObservable()
				.Select(s =>
				{
					var doc = XDocument.Parse(s);
					var ns = doc.Root.Name.Namespace;
					return doc.Descendants(ns + "Item").Select(t => t.Element(ns + "Address").Value).FirstOrDefault();
				});
		}

		/// <summary>
		/// 位置情報を元に住所を特定します。
		/// </summary>
		/// <param name="geoData"></param>
		/// <returns></returns>
		public static IObservable<string> GetAddress(GeoCoordinate geoData)
		{
			return GetAddress(geoData.Latitude, geoData.Longitude);
		}
	}
}
