﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace ImakokoWp.Imakoko
{
	public class LatestRoot
	{
		public string result { get; set; }
		public points[] points { get; set; }
	}

	public class points
	{
		public string valid { get; set; }
		public string user { get; set; }
		public string nickname { get; set; }
		public string lat { get; set; }
		public string lon { get; set; }
		public string dir { get; set; }
		public string altitude { get; set; }
		public string velocity { get; set; }
		public string type { get; set; }
		public string ustream_status { get; set; }
	}

	public class Latest
	{
		/// <summary>
		/// ユーザー ID を取得します。
		/// </summary>
		public string User { get; private set; }
		/// <summary>
		/// ニックネームを取得します。
		/// </summary>
		public string Nickname { get; private set; }
		/// <summary>
		/// ユーザー名とニックネームをあわせた名前を取得します。
		/// </summary>
		public string ShowName
		{
			get
			{
				if (string.IsNullOrWhiteSpace(this.Nickname))
					return this.User;
				return this.Nickname + " (" + this.User + ")";
			}
		}
		/// <summary>
		/// ユーザーの現在位置の緯度を取得します。
		/// </summary>
		public double Latitude { get; private set; }
		/// <summary>
		/// ユーザーの現在位置の経度を取得します。
		/// </summary>
		public double Longitude { get; private set; }
		/// <summary>
		/// 北を 0 度としたときの方位を取得します。
		/// </summary>
		public double Direction { get; private set; }
		/// <summary>
		/// 高度を取得します。
		/// </summary>
		public double Altitude { get; private set; }
		/// <summary>
		/// 速度を取得します。
		/// </summary>
		public double Speed { get; private set; }
		/// <summary>
		/// マーカーの種類を取得します。
		/// </summary>
		public MarkerType MakerType { get; private set; }
		/// <summary>
		/// 生放送中かどうか取得します。
		/// </summary>
		public string UstreamStatus { get; private set; }
		/// <summary>
		/// 自分の位置からの距離を取得します。
		/// </summary>
		public double Distance { get; private set; }
		/// <summary>
		/// 
		/// </summary>
		public string Status { get; private set; }

		public string Position
		{
			get { return Latest.FormatPosition(this.Latitude, this.Longitude); }
		}

		public Latest(string user, string nickname, string lat, string lon, string dir, string altitude, string velocity, string type, string ustream_status, double myLat, double myLon)
		{
			this.User = user;
			this.Nickname = nickname;
			this.Latitude = string.IsNullOrWhiteSpace(lat) ? 0d : double.Parse(lat);
			this.Longitude = string.IsNullOrWhiteSpace(lon) ? 0d : double.Parse(lon);
			this.Direction = string.IsNullOrWhiteSpace(dir) || dir == "NaN" ? 0f : float.Parse(dir);
			this.Altitude = string.IsNullOrWhiteSpace(altitude) ? 0f : float.Parse(altitude);
			this.MakerType = string.IsNullOrWhiteSpace(type) ? MarkerType.Arrow : (MarkerType)Enum.Parse(typeof(MarkerType), type, true);
			this.UstreamStatus = ustream_status;

			this.Distance = Latest.GetDistance(myLat, myLon, this.Latitude, this.Longitude);
			this.Status = Latest.FormatPosition(this.Latitude, this.Longitude) + ": " + this.Distance.ToString("0.00") + "km";
		}

		public static string FormatPosition(double lat, double lon)
		{
			return string.Format("{0}, {1}",
				lat > 0 ? "N" + lat.ToString("0.0000") : "S" + Math.Abs(lat).ToString("0.0000"),
				lon > 0 ? "E" + lon.ToString("0.0000") : "W" + Math.Abs(lon).ToString("0.0000"));
		}

		/// <summary>
		/// 2 点間の距離を返します。
		/// </summary>
		/// <param name="x1"></param>
		/// <param name="y1"></param>
		/// <param name="x2"></param>
		/// <param name="y2"></param>
		/// <returns></returns>
		public static double GetDistance(double x1, double y1, double x2, double y2)
		{
			//float x = Math.Abs(x2) - Math.Abs(x1);
			//float y = Math.Abs(y2) - Math.Abs(y1);

			//return (float)Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2));

			var r = 6378.137f;	// 地球半径
			var x1r = (double)x1 * Math.PI / 180f;
			var x2r = (double)x2 * Math.PI / 180f;
			var y1r = (double)y1 * Math.PI / 180f;
			var y2r = (double)y2 * Math.PI / 180f;
			var d = r * (double)Math.Acos((Math.Sin(y1r) * Math.Sin(y2r)) + (Math.Cos(y1r) * Math.Cos(y2r) * Math.Cos(x1r - x2r)));
			return d;
		}

		public override string ToString()
		{
			return this.ShowName + " " + this.Status + " Dist: " + this.Distance;
		}
	}
}
