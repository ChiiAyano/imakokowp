﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Reactive;
using System.Linq;
using ImakokoWp.Extensions;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;

namespace ImakokoWp.Imakoko
{
	/// <summary>
	/// 今ココなう！のクライアントです。
	/// </summary>
	public class ImakokoClient
	{
		readonly Uri baseUri = new Uri("http://imakoko-gps.appspot.com/api/");

		/// <summary>
		/// ユーザー名を取得または設定します。
		/// </summary>
		public string UserName { get; set; }
		/// <summary>
		/// パスワードを取得または設定します。
		/// </summary>
		public string Password { get; set; }

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="time">送信時刻</param>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="gpsq">GPS の FIX 情報</param>
		/// <param name="gpsn">衛星捕捉数</param>
		/// <param name="gpsh">高度 (メートル単位)</param>
		/// <param name="gpsd">北を 0 度としたときの方位 (度単位)</param>
		/// <param name="gpsv">速度 (キロメートル毎時)</param>
		/// <param name="t">マーカーの種類</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(DateTime time, double lat, double lon, int gpsq, int gpsn, double gpsh, double gpsd, double gpsv, MarkerType t)
		{
			var timestr = time.ToString("yyyy-MM-dd'T'HH:mm:ss.Fzzz");
			var type = (int)t;
			var uri = this.baseUri + "post?" + "time=" + timestr + "&lat=" + lat + "&lon=" + lon;

			if (gpsq != 0)
				uri += "&gpsq=" + gpsq;
			if (gpsn != 0)
				uri += "&gpsn=" + gpsn;
			if (gpsh != 0d)
				uri += "&gpsh=" + gpsh;
			if (gpsd != 0d)
				uri += "&gpsd=" + gpsd;
			if (gpsv != 0d)
				uri += "&gpsv=" + gpsv;
			if (type != 0)
				uri += "&t=" + type;

			var webreq = WebRequest.Create(uri);
			webreq.ContentType = "	application/x-www-form-urlencoded";
			webreq.Method = "POST";
			webreq.Credentials = new NetworkCredential(this.UserName, this.Password);

			var res = webreq.GetResponseStringAsObservable()
				.Select(s =>
					{
						return s == "OK" ? true : false;
					});

			return res;
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="gpsq">GPS の FIX 情報</param>
		/// <param name="gpsn">衛星捕捉数</param>
		/// <param name="gpsh">高度 (メートル単位)</param>
		/// <param name="gpsd">北を 0 度としたときの方位 (度単位)</param>
		/// <param name="gpsv">速度 (キロメートル毎時)</param>
		/// <param name="t">マーカーの種類</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon, int gpsq, int gpsn, double gpsh, double gpsd, double gpsv, MarkerType t)
		{
			return PostAsObservable(DateTime.Now, lat, lon, gpsq, gpsn, gpsh, gpsd, gpsv, t);
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="gpsq">GPS の FIX 情報</param>
		/// <param name="gpsn">衛星捕捉数</param>
		/// <param name="gpsh">高度 (メートル単位)</param>
		/// <param name="gpsd">北を 0 度としたときの方位 (度単位)</param>
		/// <param name="gpsv">速度 (キロメートル毎時)</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon, int gpsq, int gpsn, double gpsh, double gpsd, double gpsv)
		{
			return PostAsObservable(DateTime.Now, lat, lon, gpsq, gpsn, gpsh, gpsd, gpsv, MarkerType.Arrow);
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="gpsq">GPS の FIX 情報</param>
		/// <param name="gpsn">衛星捕捉数</param>
		/// <param name="gpsh">高度 (メートル単位)</param>
		/// <param name="gpsd">北を 0 度としたときの方位 (度単位)</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon, int gpsq, int gpsn, double gpsh, double gpsd)
		{
			return PostAsObservable(DateTime.Now, lat, lon, gpsq, gpsn, gpsh, gpsd, 0f, MarkerType.Arrow);
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="gpsq">GPS の FIX 情報</param>
		/// <param name="gpsn">衛星捕捉数</param>
		/// <param name="gpsh">高度 (メートル単位)</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon, int gpsq, int gpsn, double gpsh)
		{
			return PostAsObservable(DateTime.Now, lat, lon, gpsq, gpsn, gpsh, 0d, 0f, MarkerType.Arrow);
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="gpsq">GPS の FIX 情報</param>
		/// <param name="gpsn">衛星捕捉数</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon, int gpsq, int gpsn)
		{
			return PostAsObservable(DateTime.Now, lat, lon, gpsq, gpsn, 0f, 0d, 0f, MarkerType.Arrow);
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="gpsq">GPS の FIX 情報</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon, int gpsq)
		{
			return PostAsObservable(DateTime.Now, lat, lon, gpsq, 0, 0f, 0d, 0f, MarkerType.Arrow);
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon)
		{
			return PostAsObservable(DateTime.Now, lat, lon, 0, 0, 0f, 0d, 0f, MarkerType.Arrow);
		}

		/// <summary>
		/// 座標データをサーバーに転送します。
		/// </summary>
		/// <param name="lat">緯度 (度単位)</param>
		/// <param name="lon">経度 (度単位)</param>
		/// <param name="t">マーカーの種類</param>
		/// <returns></returns>
		public IObservable<bool> PostAsObservable(double lat, double lon, MarkerType t)
		{
			return PostAsObservable(DateTime.Now, lat, lon, 0, 0, 0f, 0d, 0f, t);
		}

		public IObservable<bool> PostAsObservable(double lat, double lon, double gpsh, double gpsd, double gpsv, MarkerType t)
		{
			return PostAsObservable(DateTime.Now, lat, lon, 0, 0, gpsh, gpsd, gpsv, t);
		}

		/// <summary>
		/// 現在のユーザー位置情報を取得します。
		/// </summary>
		/// <param name="user"></param>
		/// <param name="lat"></param>
		/// <param name="lon"></param>
		/// <returns></returns>
		public IObservable<Latest> LatestAsObservable(double lat, double lon, string user = "all")
		{
			var uri = this.baseUri + "latest?user=" + user;
			var webreq = WebRequest.Create(uri);
			webreq.Method = "POST";

			var res = webreq.GetResponseAsObservable()
				.Select(s => GetApiData<LatestRoot>(s.GetResponseStream()))
				.SelectMany(s => s.points)
				.Select(s => new Latest(s.user, s.nickname, s.lat, s.lon, s.dir, s.altitude, s.velocity, s.type, s.ustream_status, lat, lon))
				.Where(w => w.User != this.UserName);
			return res;
		}

		/// <summary>
		/// 座標から住所を取得します。
		/// </summary>
		/// <param name="lat"></param>
		/// <param name="lon"></param>
		/// <returns></returns>
		public IObservable<string> GetAddressAsObservable(double lat, double lon)
		{
			var uri = this.baseUri + "getaddress?lat=" + lat + "&lon=" + lon;
			var webreq = WebRequest.Create(uri);
			webreq.Method = "GET";
			webreq.Credentials = new NetworkCredential(this.UserName, this.Password);

			var res = webreq.GetResponseStringAsObservable();
			return res;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="responseStream"></param>
		/// <returns></returns>
		private T GetApiData<T>(Stream responseStream)
		{
			using (var sr = new StreamReader(responseStream))
			{
				var text = sr.ReadToEnd();
				text = text.Trim('(', ')');

				var bytes = Encoding.UTF8.GetBytes(text);
				using (var ms = new MemoryStream(bytes))
				{
					var serial = new DataContractJsonSerializer(typeof(T));
					return (T)serial.ReadObject(ms);
				}
			}
		}
	}

	/// <summary>
	/// マーカーの種類を定義します。
	/// </summary>
	public enum MarkerType
	{
		/// <summary>
		/// 矢印を表示します。
		/// </summary>
		Arrow,
		/// <summary>
		/// 携帯電話マーカーを表示します。
		/// </summary>
		Phone,
		/// <summary>
		/// 飛行機マーカーを表示します。
		/// </summary>
		AirPlane,
		/// <summary>
		/// 電車マーカーを表示します。
		/// </summary>
		Train,
		/// <summary>
		/// 新幹線マーカーを表示します。
		/// </summary>
		Shinkansen,
		/// <summary>
		/// バス マーカーを表示します。
		/// </summary>
		Bus,
		/// <summary>
		/// 自転車マーカーを表示します。
		/// </summary>
		Bicycle,
		/// <summary>
		/// 徒歩マーカーを表示します。
		/// </summary>
		Walk,
		/// <summary>
		/// バイク マーカーを表示します。
		/// </summary>
		Motorcycle,
		/// <summary>
		/// ヘリコプター マーカーを表示します。
		/// </summary>
		Helicopter,
		/// <summary>
		/// 船マーカーを表示します。
		/// </summary>
		Ship,
		/// <summary>
		/// Twitter の画像をマーカーとして表示します。
		/// </summary>
		Twitter = 99
	}
}
